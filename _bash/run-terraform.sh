#! /usr/bin/env bash

SCRIPT_DIR="$(cd "$(dirname "$0")" > /dev/null 2>&1 && pwd)"

cd $SCRIPT_DIR/../

for DIR in tf_modules/*; do
    if [ -d "$DIR" ]; then
        cd "$DIR"
        terraform init 
        terraform plan 2>&1 | tee -a $SCRIPT_DIR/../sample-out.txt 
        cd -
        # I don't run terraform apply to prevent incurrence of undersired costs
    fi
done

cd -

