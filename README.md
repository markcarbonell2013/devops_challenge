
# DevOps Challenge

# Answer to challenge questions and preliminary considerations

[The DevOps Challenge Wiki](https://gitlab.com/markcarbonell2013/devops_challenge/-/wikis/home)
[The Answer Page](https://gitlab.com/markcarbonell2013/devops_challenge/-/wikis/devops-challenge-answer)

# Setup

1. Pull the empty submodules
```bash
git submodule update --init
```

2. Install terraform `v1.1.5` in your os (preferably the Linux AMI)

3. To see the output I produced with my AWS account do

```bash
cat sample-output.txt
```

4. To reproduce that output yourself you have to: 

- Create an aws programatic account in IAM and save the accounts AWS Access Key and AWS Secret Key
- For testing purposes, attach the following IAM policies to your user account (remember to remove them after you are done testing)
    - AmazonEC2FullAccess
    - AmazonEC2ContainerRegistryFullAccess
    - AmazonVPCFullAccess
- After setting up AWS, add your AWS Access key and AWS Secret key to the `.tfvars` file in each module

```bash
# for staging env
cd tf_modules/environment_staging/
cp terraform.tfvars.example terraform.tfvars
vim terraform.tfvars # add your AWS keys there

# for testing env 
cd tf_modules/environment_staging/
cp terraform.tfvars.example terraform.tfvars
vim terraform.tfvars # add your AWS keys there

# for production env 
cp tf_modules/environment_staging/
cp terraform.tfvars.example terraform.tfvars
vim terraform.tfvars # add your AWS keys there
```

5. Once the previous steps are completed you can test the terraform generation plans for all three environments with

```bash
sh _bash/run-terraform.sh
```

6. You can compare the output you get with the `sample-output.txt` file stored in the project's root.
