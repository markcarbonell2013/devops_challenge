
resource "aws_vpc" "test_vpc" {
  cidr_block       = "${var.aws_vpc_cidr}"
  instance_tenancy = "default"
  tags = {
    Name = "Test VPC"
  }
}

