
resource "aws_instance" "test_web_instance" {
  ami = "ami-05b308c240ae70bb6"
  instance_type = "t2.micro"
  tags = {
    Name = "Test Instance"
  }

  network_interface {
    network_interface_id = aws_network_interface.test_network_interface_1.id
    device_index = 0
  }

  network_interface {
    network_interface_id = aws_network_interface.test_network_interface_2.id
    device_index = 0
  }

  network_interface {
    network_interface_id = aws_network_interface.test_network_interface_3.id
    device_index = 0
  }


  credit_specification {
    cpu_credits = "unlimited"
  }
}
