
variable "aws_region" {
  default = "no-existing-region"
  type = string
  description = "AWS Region"
}

variable "aws_access_key" {
  default = "no-existing-access-key"
  type = string
  description = "AWS Access Key"
}

variable "aws_secret_key" {
  default = "no-existing-secret-key"
  type = string
  description = "AWS Access Key"
}

variable "aws_availability_zones" {
  default = ["non-existing-availability-zones"]
  type = list(string)
  description = "AWS Availability Zones (3)"
}

variable "aws_subnet_cidr" {
  default = ["non-existing-subnet-cidr"]
  type = list(string)
  description = "AWS Subnet CIDR (3)"
}

variable "aws_vpc_cidr" {
  default = "non-existing-vpc-cidr"
  type = string
  description = "AWS VPC CIDR"
}
