resource "aws_internet_gateway" "test_gateway" {
  vpc_id = "${aws_vpc.test_vpc.id}"
}

resource "aws_subnet" "test_subnet_1" {
  vpc_id                  = "${aws_vpc.test_vpc.id}"
  cidr_block              = element(var.aws_subnet_cidr, 1)
  map_public_ip_on_launch = true
  availability_zone = element(var.aws_availability_zones, 1)
  tags = {
    Name = "Test Subnet 1"
  }
}

resource "aws_subnet" "test_subnet_2" {
  vpc_id                  = "${aws_vpc.test_vpc.id}"
  cidr_block              = element(var.aws_subnet_cidr, 2)
  map_public_ip_on_launch = true
  availability_zone = element(var.aws_availability_zones, 2)
  tags = {
    Name = "Test Subnet 2"
  }
}

resource "aws_subnet" "test_subnet_3" {
  vpc_id                  = "${aws_vpc.test_vpc.id}"
  cidr_block              = element(var.aws_subnet_cidr, 3)
  map_public_ip_on_launch = true
  availability_zone = element(var.aws_availability_zones, 3)
  tags = {
    Name = "Test Subnet 3"
  }
}

resource "aws_route_table" "test_route" {
  vpc_id = "${aws_vpc.test_vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.test_gateway.id}"
  }

  tags = {
    Name = "Route to internet"
  }
}

resource "aws_route_table_association" "test_rta1" {
  subnet_id = "${aws_subnet.test_subnet_1.id}"
  route_table_id = "${aws_route_table.test_route.id}"
}

resource "aws_route_table_association" "test_rta2" {
  subnet_id = "${aws_subnet.test_subnet_2.id}"
  route_table_id = "${aws_route_table.test_route.id}"
}

resource "aws_route_table_association" "test_rta3" {
  subnet_id = "${aws_subnet.test_subnet_3.id}"
  route_table_id = "${aws_route_table.test_route.id}"
}

resource "aws_network_interface" "test_network_interface_1" {
  subnet_id   = aws_subnet.test_subnet_1.id
  private_ips = ["172.16.10.100"]

  tags = {
    Name = "primary_network_interface"
  }
}

resource "aws_network_interface" "test_network_interface_2" {
  subnet_id   = aws_subnet.test_subnet_2.id
  private_ips = ["172.16.10.101"]

  tags = {
    Name = "secondary_network_interface"
  }
}

resource "aws_network_interface" "test_network_interface_3" {
  subnet_id   = aws_subnet.test_subnet_3.id
  private_ips = ["172.16.10.102"]

  tags = {
    Name = "tertiary_network_interface"
  }
}
