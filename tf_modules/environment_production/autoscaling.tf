resource "aws_security_group" "prod_sg2" {
  name        = "Prod Security Group 2"
  description = "A security group for the elastic load balancer"
  vpc_id      = "${aws_vpc.prod_vpc.id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_autoscaling_group" "prod_autoscaling_group" {
  name = "${aws_launch_configuration.prod_web_launch_config.name}-asg"
  min_size             = 1
  desired_capacity     = 1
  max_size             = 3

  health_check_type    = "ELB"
  load_balancers = [
    "${aws_elb.prod_web_elb.id}"
  ]
  launch_configuration = "${aws_launch_configuration.prod_web_launch_config.name}"
  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupTotalInstances"
  ]
  metrics_granularity = "1Minute"
  vpc_zone_identifier  = [
    "${aws_subnet.prod_subnet_1.id}",
    "${aws_subnet.prod_subnet_2.id}",
    "${aws_subnet.prod_subnet_3.id}"
  ]
  # Required to redeploy without an outage.
  lifecycle {
    create_before_destroy = true
  }
  tag {
    key                 = "Name"
    value               = "Prod Web Autoscaling Group"
    propagate_at_launch = true
  }
}

resource "aws_autoscaling_policy" "prod_web_policy_down" {
  name = "prod_web_policy_down"
  scaling_adjustment = -1
  adjustment_type = "ChangeInCapacity"
  cooldown = 300
  autoscaling_group_name = "${aws_autoscaling_group.prod_autoscaling_group.name}"
}

resource "aws_autoscaling_policy" "prod_web_policy_up" {
  name = "prod_web_policy_up"
  scaling_adjustment = 1
  adjustment_type = "ChangeInCapacity"
  cooldown = 300
  autoscaling_group_name = "${aws_autoscaling_group.prod_autoscaling_group.name}"
}
