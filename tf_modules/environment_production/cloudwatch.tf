
resource "aws_cloudwatch_metric_alarm" "prod_web_cpu_alarm_up" {
  alarm_name = "prod_web_cpu_alarm_up"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = "120"
  statistic = "Average"
  threshold = "80"
  dimensions = {
    AutoScalingGroupName = "${aws_autoscaling_group.prod_autoscaling_group.name}"
  }
  alarm_description = "This metric monitor EC2 instance CPU utilization"
  alarm_actions = [ "${aws_autoscaling_policy.prod_web_policy_up.arn}" ]
}


resource "aws_cloudwatch_metric_alarm" "prod_web_cpu_alarm_down" {
  alarm_name = "prod_web_cpu_alarm_down"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = "120"
  statistic = "Average"
  threshold = "30"
  dimensions = {
    AutoScalingGroupName = "${aws_autoscaling_group.prod_autoscaling_group.name}"
  }
  alarm_description = "This metric monitor EC2 instance CPU utilization"
  alarm_actions = [ "${aws_autoscaling_policy.prod_web_policy_down.arn}" ]
}
