resource "aws_internet_gateway" "prod_gateway" {
  vpc_id = "${aws_vpc.prod_vpc.id}"
}

resource "aws_subnet" "prod_subnet_1" {
  vpc_id                  = "${aws_vpc.prod_vpc.id}"
  cidr_block              = element(var.aws_subnet_cidr, 1)
  map_public_ip_on_launch = true
  availability_zone = element(var.aws_availability_zones, 1)
  tags = {
    Name = "Prod Subnet 1"
  }
}

resource "aws_subnet" "prod_subnet_2" {
  vpc_id                  = "${aws_vpc.prod_vpc.id}"
  cidr_block              = element(var.aws_subnet_cidr, 2)
  map_public_ip_on_launch = true
  availability_zone = element(var.aws_availability_zones, 2)
  tags = {
    Name = "Prod Subnet 2"
  }
}

resource "aws_subnet" "prod_subnet_3" {
  vpc_id                  = "${aws_vpc.prod_vpc.id}"
  cidr_block              = element(var.aws_subnet_cidr, 3)
  map_public_ip_on_launch = true
  availability_zone = element(var.aws_availability_zones, 3)
  tags = {
    Name = "Prod Subnet 3"
  }
}

resource "aws_route_table" "prod_route" {
  vpc_id = "${aws_vpc.prod_vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.prod_gateway.id}"
  }

  tags = {
    Name = "Route to internet"
  }
}

resource "aws_route_table_association" "prod_rta1" {
  subnet_id = "${aws_subnet.prod_subnet_1.id}"
  route_table_id = "${aws_route_table.prod_route.id}"
}

resource "aws_route_table_association" "prod_rta2" {
  subnet_id = "${aws_subnet.prod_subnet_2.id}"
  route_table_id = "${aws_route_table.prod_route.id}"
}

resource "aws_route_table_association" "prod_rta3" {
  subnet_id = "${aws_subnet.prod_subnet_3.id}"
  route_table_id = "${aws_route_table.prod_route.id}"
}


