
resource "aws_vpc" "prod_vpc" {
  cidr_block       = "${var.aws_vpc_cidr}"
  instance_tenancy = "default"
  tags = {
    Name = "Prod VPC"
  }
}

