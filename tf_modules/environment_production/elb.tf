resource "aws_security_group" "prod_sg1" {
  name        = "Prod Security Group"
  description = "Prod Module"
  vpc_id      = "${aws_vpc.prod_vpc.id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_elb" "prod_web_elb" {
  name = "prod-web-elb"
  security_groups = [
    "${aws_security_group.prod_sg1.id}"
  ]
  subnets = [
    "${aws_subnet.prod_subnet_1.id}",
    "${aws_subnet.prod_subnet_2.id}",
    "${aws_subnet.prod_subnet_3.id}"
  ]
  cross_zone_load_balancing   = true
  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    interval = 30
    target = "HTTP:80/"
  }
  listener {
    lb_port = 80
    lb_protocol = "http"
    instance_port = "80"
    instance_protocol = "http"
  }
}

