
resource "aws_vpc" "staging_vpc" {
  cidr_block       = "${var.aws_vpc_cidr}"
  instance_tenancy = "default"
  tags = {
    Name = "Staging VPC"
  }
}

