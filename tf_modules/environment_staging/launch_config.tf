resource "aws_launch_configuration" "staging_web_launch_config" {
  name_prefix = "web-"
  image_id = "ami-05b308c240ae70bb6" # ubuntu 
  instance_type = "t2.micro"
  key_name = "tests"
  security_groups = [ "${aws_security_group.staging_sg1.id}" ]
  associate_public_ip_address = true
  user_data = "${file("get_staging_data.sh")}"
  lifecycle {
    create_before_destroy = true
  }
}

